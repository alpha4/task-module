package com.bookbuf.service;

import com.bookbuf.bean.Task;
import com.bookbuf.bean.User;

import java.util.List;

/**
 * 任务
 */
public interface TaskService {

    boolean insert(User user, Task task);

    boolean update(User user, Task task);

    boolean delete(User user, Task task);

    List<Task> query(User user, String condition);
}
