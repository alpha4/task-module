package com.bookbuf.service.impl;

import com.bookbuf.bean.Task;
import com.bookbuf.bean.User;
import com.bookbuf.mockdb.MockTaskDatabase;
import com.bookbuf.service.TaskService;

import java.util.List;

/**
 * Created by robert on 16/7/23.
 */
public class TaskServiceImpl implements TaskService {

    private static TaskServiceImpl instance = new TaskServiceImpl();

    private TaskServiceImpl() {
    }

    public static TaskServiceImpl getInstance() {
        return instance;
    }

    private static MockTaskDatabase database = new MockTaskDatabase();

    @Override
    public boolean insert(User user, Task task) {
        return database.insert(user, task);
    }

    @Override
    public boolean update(User user, Task task) {
        return database.update(user, task);
    }

    @Override
    public boolean delete(User user, Task task) {
        return database.delete(user, task);
    }

    @Override
    public List<Task> query(User user, String condition) {
        return database.query(user, condition);
    }
}
