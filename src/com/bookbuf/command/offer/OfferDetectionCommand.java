package com.bookbuf.command.offer;

import com.bookbuf.bean.User;
import com.bookbuf.command.Command;

/**
 * 当检测被触发时
 */
public class OfferDetectionCommand implements Command {
    @Override
    public void execute(User user) {
        System.out.println("会员" + user.getRealname() + "加入1条检测任务,3天后生效.");
    }

    @Override
    public void log(User user) {
        System.out.println("日志:会员" + user.getRealname() + "加入1条检测任务,3天后生效.");
    }
}
