package com.bookbuf.command.offer;

import com.bookbuf.bean.Task;
import com.bookbuf.bean.User;
import com.bookbuf.command.Command;
import com.bookbuf.command.LevelActiveCommand;
import com.bookbuf.command.processor.CommandProcessor;
import com.bookbuf.service.TaskService;
import com.bookbuf.service.impl.TaskServiceImpl;

/**
 * 当开卡被触发时
 */
public class OfferOpenCardCommand implements Command {
    @Override
    public void execute(User user) {

        Task newTask = new Task(
                Task.Configuration.newInstance(2, "回访任务", "给[" + user.getRealname() + "]回访", "2016-7-26", "2016-7-27"),
                Task.Stat.newInstance(1, 0)
        );

        TaskServiceImpl.getInstance().insert(user, newTask);

        CommandProcessor.run(user, new LevelActiveCommand());
    }

    @Override
    public void log(User user) {
        System.out.println("日志:用户" + user.getRealname() + "增加1次开卡任务,2天后生效");
    }
}
