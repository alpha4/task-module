package com.bookbuf.command;

import com.bookbuf.bean.User;
import com.bookbuf.command.Command;

/**
 * 将会员等级分配为  活跃
 */
public class LevelActiveCommand implements Command {
    @Override
    public void execute(User user) {
        System.out.println("激活会员" + user.getRealname() + "为活跃会员");
    }

    @Override
    public void log(User user) {
        System.out.println("日志:激活会员" + user.getRealname() + "为活跃会员");
    }
}
