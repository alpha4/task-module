package com.bookbuf.command.poll;

import com.bookbuf.bean.User;
import com.bookbuf.command.Command;

/**
 * 开卡任务完成时
 */
public class PollOpenCardCommand implements Command {
    @Override
    public void execute(User user) {
        System.out.println("用户" + user.getRealname() + "完成1次开卡任务");
    }

    @Override
    public void log(User user) {
        System.out.println("日志:用户" + user.getRealname() + "完成1次开卡任务");
    }
}
