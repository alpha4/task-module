package com.bookbuf.command.poll;

import com.bookbuf.bean.User;
import com.bookbuf.command.Command;

/**
 * 检测任务完成时
 */
public class PollDetectionCommand implements Command {
    @Override
    public void execute(User user) {
        System.out.println("会员" + user.getRealname() + "完成1次检测任务");
    }

    @Override
    public void log(User user) {
        System.out.println("日志:会员" + user.getRealname() + "完成1次检测任务");
    }
}
