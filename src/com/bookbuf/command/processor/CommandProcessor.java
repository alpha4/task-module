package com.bookbuf.command.processor;

import com.bookbuf.bean.User;
import com.bookbuf.command.Command;

/**
 * Created by robert on 16/7/23.
 */
public class CommandProcessor {

    public static void run(User user, Command command) {
        command.execute(user);
    }
}
