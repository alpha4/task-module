package com.bookbuf.command;

import com.bookbuf.bean.User;

/**
 * 将会员等级分配为  沉默
 */
public class LevelSilenceCommand implements Command {
    @Override
    public void execute(User user) {
        System.out.println("会员" + user.getRealname() + "调为沉默用户");
    }

    @Override
    public void log(User user) {
        System.out.println("日志:会员" + user.getRealname() + "调为沉默用户");
    }
}
