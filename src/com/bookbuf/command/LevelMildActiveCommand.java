package com.bookbuf.command;

import com.bookbuf.bean.User;
import com.bookbuf.command.Command;

/**
 * 将会员等级分配为  轻度活跃
 */
public class LevelMildActiveCommand implements Command {
    @Override
    public void execute(User user) {
        System.out.println("会员" + user.getRealname() + "调为轻度活跃用户");
    }

    @Override
    public void log(User user) {
        System.out.println("日志:会员" + user.getRealname() + "调为轻度活跃用户");
    }
}
