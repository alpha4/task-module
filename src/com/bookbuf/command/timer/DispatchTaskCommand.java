package com.bookbuf.command.timer;

import com.bookbuf.bean.User;
import com.bookbuf.command.Command;

// TODO: 16/7/23 任务总表 表示任务池
// TODO: 16/7/23 任务日志表 跟踪每日任务完成情况

/**
 * 如凌晨三点, 当昨天任务核算完毕后.
 * 按照某算法,分配今天的任务
 */
public class DispatchTaskCommand implements Command {
    @Override
    public void execute(User user) {

    }

    @Override
    public void log(User user) {

    }
}
