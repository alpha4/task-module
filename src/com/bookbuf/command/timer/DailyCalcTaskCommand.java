package com.bookbuf.command.timer;

import com.bookbuf.bean.User;
import com.bookbuf.command.Command;

/**
 * 每日任务核算, 比如凌晨3点核算昨天的任务进度,并将未完成的任务重新入库表
 */
public class DailyCalcTaskCommand implements Command {
    @Override
    public void execute(User user) {
        System.out.println("核算用户" + user.getRealname() + "2016-07-23的任务完成情况");
    }

    @Override
    public void log(User user) {
        System.out.println("日志:核算用户" + user.getRealname() + "2016-07-23的任务完成情况");
    }
}
