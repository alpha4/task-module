package com.bookbuf.command;

import com.bookbuf.bean.User;
import com.bookbuf.service.impl.TaskServiceImpl;

/**
 * Created by robert on 16/7/23.
 */
public interface Command {

    void execute(User user);

    @Deprecated
    void log(User user);
}
