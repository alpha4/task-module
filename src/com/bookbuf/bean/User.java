package com.bookbuf.bean;

/**
 * Created by robert on 16/7/23.
 */
public class User {

    private int id = 999;
    private String realname = "陈俊棋";
    private String mobile = "18668247775";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", realname='" + realname + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
