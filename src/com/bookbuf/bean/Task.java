package com.bookbuf.bean;


/**
 * 任务
 */
public class Task {

    private int id = -1; // 自增 id
    private String complete = null;// 任务完成时间

    private Configuration configuration = null;
    private Stat stat = null;

    public Task(Configuration configuration, Stat stat) {
        this.configuration = configuration;
        this.stat = stat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public Stat getStat() {
        return stat;
    }

    public void setStat(Stat stat) {
        this.stat = stat;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", complete=" + complete +
                ", configuration=" + configuration +
                ", stat=" + stat +
                '}';
    }

    /**
     * 任务基本配置
     */
    public static class Configuration {
        private int type;// 任务类型 -- 检测/回访/开卡/培训..
        private String title;// 任务标题
        private String description;// 任务描述
        private String begin; // 任务开始时间
        private String end;  // 任务结束时间

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getBegin() {
            return begin;
        }

        public void setBegin(String begin) {
            this.begin = begin;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        public static Configuration newInstance(int type, String title, String desc, String begin, String end) {
            Configuration configuration = new Configuration();
            configuration.setType(type);
            configuration.setTitle(title);
            configuration.setDescription(desc);
            configuration.setBegin(begin);
            configuration.setEnd(end);
            return configuration;
        }

        @Override
        public String toString() {
            return "Configuration{" +
                    "type=" + type +
                    ", title='" + title + '\'' +
                    ", description='" + description + '\'' +
                    ", begin=" + begin +
                    ", end=" + end +
                    '}';
        }
    }

    /**
     * 任务统计
     */
    public static class Stat {
        private int total = 1; // 总任务数
        private int execute = 0; // 已执行任务数

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getExecute() {
            return execute;
        }

        public void setExecute(int execute) {
            this.execute = execute;
        }

        public int getUnExecute() {
            return total - execute;
        }

        public int getExecutePercent() {
            return (int) (execute / total * 1.0) * 100;
        }

        public static Stat newInstance(int total, int execute) {
            Stat stat = new Stat();
            stat.total = total;
            stat.execute = execute;
            return stat;
        }

        @Override
        public String toString() {
            return "Stat{" +
                    "total=" + total +
                    ", execute=" + execute +
                    '}';
        }
    }
}
