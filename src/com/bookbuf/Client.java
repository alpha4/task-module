package com.bookbuf;

import com.bookbuf.bean.Task;
import com.bookbuf.bean.User;
import com.bookbuf.command.LevelActiveCommand;
import com.bookbuf.command.offer.OfferDetectionCommand;
import com.bookbuf.command.offer.OfferOpenCardCommand;
import com.bookbuf.command.processor.CommandProcessor;
import com.bookbuf.service.impl.TaskServiceImpl;

public class Client {

    private static User user = new User();

    public static void main(String[] args) {
        configurationTasks();

        mockForgroundCall();
    }

    public static void mockForgroundCall() {

        System.out.println("模拟客户端调用 +");
        CommandProcessor.run(user, new OfferOpenCardCommand());// 在开卡指令内部需关联 LevelActiveCommand
        System.out.println("模拟客户端调用 -");
    }


    // 后台配置任务
    public static void configurationTasks() {

        System.out.println("后台配置任务 +");

        Task task1 = new Task(
                Task.Configuration.newInstance(1, "检测任务", "检测血压5次", "2016-7-23", "2016-7-24"),
                Task.Stat.newInstance(5, 0)

        );
        Task task2 = new Task(
                Task.Configuration.newInstance(1, "检测任务", "检测总胆固醇5次", "2016-7-23", "2016-7-24"),
                Task.Stat.newInstance(5, 0)

        );
        Task task3 = new Task(
                Task.Configuration.newInstance(2, "回访任务", "给张三回访", "2016-7-23", "2016-7-24"),
                Task.Stat.newInstance(1, 0)

        );
        Task task4 = new Task(
                Task.Configuration.newInstance(2, "回访任务", "给李四回访", "2016-7-23", "2016-7-24"),
                Task.Stat.newInstance(1, 0)

        );
        Task task5 = new Task(
                Task.Configuration.newInstance(3, "开卡任务", "开卡3次", "2016-7-23", "2016-7-24"),
                Task.Stat.newInstance(1, 0)

        );
        TaskServiceImpl taskService = TaskServiceImpl.getInstance();
        taskService.insert(user, task1);
        taskService.insert(user, task2);
        taskService.insert(user, task3);
        taskService.insert(user, task4);
        taskService.insert(user, task5);


        taskService.query(user, "默认查所有记录");
        System.out.println("后台配置任务 -");
    }
}
